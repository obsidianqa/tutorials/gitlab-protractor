var HtmlScreenshotReporter = require('protractor-jasmine2-screenshot-reporter');
var htmlReporter = new HtmlScreenshotReporter({
    dest: 'test-results/',
    captureOnlyFailedSpecs: false,
    pathBuilder: function(currentSpec, suites, browserCapabilities) {
        return browserCapabilities.get('browserName') + '/screenshots/' + currentSpec.description;
      }
  });

exports.config = {
    seleniumAddress: 'http://selenium__standalone-chrome:4444/wd/hub/',
    specs: ['todo-spec.js'],
    capabilities: {
        browserName: 'chrome',
        chromeOptions: {
           args: [ "--headless", "--disable-gpu"]
         }
    },  
    onPrepare: function() {    
            var jasmineReporters = require('jasmine-reporters');
            return browser.getProcessedConfig().then(function(config) {
                var browserName = config.capabilities.browserName;
    
                var junitReporter = new jasmineReporters.JUnitXmlReporter({
                    consolidateAll: true,
                    savePath: 'test-results/' + browserName,
                    filePrefix: 'test-run'
                });
                jasmine.getEnv().addReporter(junitReporter);
                jasmine.getEnv().addReporter(htmlReporter);
        });
        
    },
};